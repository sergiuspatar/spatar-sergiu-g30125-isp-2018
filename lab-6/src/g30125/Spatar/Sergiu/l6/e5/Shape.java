/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l6.e5;

import java.awt.Color;
import java.awt.Graphics;

public abstract class Shape {

   
	private Color color;
    private int x,y;
    private String id;
    private boolean fill;
    
    public Shape(Color color,int x,int y,String id,boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }

    public Color getColor() {
        return color;
    }
    public int getx(){
    	return x;
    }
    public int gety(){
    	return y;
    }
    public String getID(){
    	return id;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public boolean getFill(){
    	return fill;
    }

    public abstract void draw(Graphics g);
  
}
