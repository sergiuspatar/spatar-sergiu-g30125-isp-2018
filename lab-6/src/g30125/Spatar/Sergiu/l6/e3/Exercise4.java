/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l6.e3;

public class Exercise4 implements CharSequence {
	private char[] sir;
	public Exercise4(char[] sir) {
		// TODO Auto-generated constructor stub
		this.sir=sir;
	}

	@Override
	public char charAt(int i) {
		// TODO Auto-generated method stub
		if(i>-1 && i<sir.length)
			return sir[i];
		return 0;
	}

	@Override
	public int length() {
		// TODO Auto-generated method stub
		return sir.length;
	}

	@Override
	public String toString() {
		return new String(sir);
	}

	@Override
	public CharSequence subSequence(int i, int j) {
		// TODO Auto-generated method stub
		char[] subsir=new char[j-j+1];
		int x=0;
		for(int y=i;y<=j;y++) 
			subsir[x++]=sir[y];
		return new Exercise4(subsir);

	}
}
