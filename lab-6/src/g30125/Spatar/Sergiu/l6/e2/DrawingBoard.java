/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l6.e2;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class DrawingBoard  extends JFrame {
	private String id;
	
    public String getId() {
		return id;
	}
	Shape[] shapes = new Shape[10];
    //ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }
    }
        public void deleteById(String id){
    	   for(int i=0;i<shapes.length;i++){
    		   if(shapes[i]!=null && shapes[i].getId().equals(id)) {
    			   shapes[i]=null;
    		   }
    	   }
       //  shapes.add(s1);
        this.repaint();
    }

    @Override
	public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
//        for(Shape s:shapes)
//            s.draw(g);
    }
}
