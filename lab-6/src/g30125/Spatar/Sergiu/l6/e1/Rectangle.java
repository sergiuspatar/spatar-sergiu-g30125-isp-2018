/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l6.e1;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle extends Shape{

    private int length;
    
    public Rectangle(Color color, String id, int x, int y, int length, boolean fill){
        super(color,id,x,y,fill);
        this.length = length;
    }
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill()==false) {
        g.drawRect(getX(),getY(),length,length);}
        else g.fillRect(getX(), getY(), length, length);
       
    }
}
