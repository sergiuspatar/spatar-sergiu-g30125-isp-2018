/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l7.e3;

import g30125.Spatar.Sergiu.l7.e3.Bank;

public class Main {
	
	public static void main(String[] args) {
		Bank bank = new Bank();
		bank.addAccount("Sergiu", 111.33);
		bank.addAccount("Robi", 102.21);
		bank.addAccount("Georgi", 204.65);
		bank.addAccount("Bety", 199.12);
		
		bank.printAccounts();
		bank.printAccounts(42,121);
		bank.getAccount("Narcis");
		bank.getAllAccounts();
	}

}