/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l7.e4;

public class Main {
	
	public static void main(String[] args) {
		Dictionary d = new Dictionary();
		Word cuv = new Word("leu");
		Definition def = new Definition("animal");		
		Word cuv1 = new Word("Audi");
		Definition def1 = new Definition("masina");	
		Word cuv2 = new Word("portocala");
		Definition def2 = new Definition("fruct");	
		d.addWord(cuv,def);
		d.addWord(cuv1,def1);
		d.addWord(cuv2,def2);
		
		d.getAllDefinition();
		d.getDefinition(cuv);
		d.getAllWords();
	}
}