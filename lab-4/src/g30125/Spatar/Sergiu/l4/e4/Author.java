/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l4.e4;
public class Author {
		
	private String name,email;
	private char gender;
	
	public Author(String name, String email, char gender)
	{
		this.name=name;
		this.email=email;
		this.gender=gender;
		
	}
	
	public String getN() {
		return name;
	}
	
	public String getE() {
		return email;
	}
	
	public char getG() {
		return gender;
	}
	
	
	public void setE() {
		this.email=email;
	}
	
	public String toString() {
		return "Author "+name+" ( "+gender+" ) at email "+email;
	}
}
