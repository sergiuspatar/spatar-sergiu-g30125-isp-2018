/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l4.e8;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class TestShape {
	@Test
	public void testShape() {
		Shape s =new Shape();
		
		assertEquals("A Shape with green and true",s.toString());
	}
	@Test
	public void testRectangle() {
		Rectangle r = new Rectangle();

		assertEquals(1.0,r.getArea(),0.01);
		assertEquals(2.0,r.getPerimeter(),0.01);
	}
	@Test
	public void testSquare() {
		
		Square sq = new Square(3.0);
		
		assertEquals(3.0,sq.getSide(),0.01);
	}
	@Test
	public void testCircle() {
		
		Circle c = new Circle();
		
		assertEquals(3.141,c.getArea(),0.01);
		assertEquals(6.282,c.getPerimeter(),0.01);
		
	}
	
}
