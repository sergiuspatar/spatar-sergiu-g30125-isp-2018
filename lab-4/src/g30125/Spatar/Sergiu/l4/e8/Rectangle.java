/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l4.e8;
public class Rectangle extends Shape {
	
	private double width,length;
	
	public Rectangle() {
		width=length=1.0;
	}
	
	public Rectangle(double width,double length) {
		this.width=width;
		this.length=length;
	}
	
	public Rectangle(double width,double length,String color,boolean filled) {
		super(color,filled);
		this.width=width;
		this.length=length;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(double width) {
		this.width=width;
	}
	
	public double getLength() {
		return length;
	}
	
	public void setLength(double length) {
		this.length=length;
	}
	
	public double getArea() {
		return width*length;
	}
	
	public double getPerimeter() {
		return 2*(width*length);
	}
	
	public String toString() {
		super.toString();
		return "A Rectangle with width="+getWidth()+" and length="+getLength()+",which is a subclas of "+super.toString()+" method from the superclass";
	}
}
