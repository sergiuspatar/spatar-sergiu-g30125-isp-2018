/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l4.e3;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class TestCircle {
	 @Test
	    public void shouldPrintArea(){
	        Circle c = new Circle(10);
	        
	        assertEquals(314.159, c.getArea(),0.001);
	 }
	 @Test
	    public void shouldPrintArea2(){
	        Circle c = new Circle();
	        
	        assertEquals(3.1415, c.getArea(),0.001);
	 }
}
