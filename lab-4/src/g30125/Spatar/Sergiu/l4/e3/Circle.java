/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l4.e3;
public class Circle {

	private double radius;
	private String color;
	
	public Circle()
	{
		radius = 1.0;
		color ="red";
	}
	
	public Circle(double radius)
	{
		this.radius=radius;
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public double getArea() {
		return Math.PI*(this.radius)*(this.radius);
	}
}
