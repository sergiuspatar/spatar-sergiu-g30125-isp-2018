/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l4.e1;

/**
 *
 * @author Sergiu
 */
public class Box {
    private int id;

    public Box(Conveyor target, int pos, int id){
        this.id = id;
        target.addPackage(this,pos);
    }

    public int getId() {
        return id;
    }

    public String toString(){
        return ""+id;
    }
}