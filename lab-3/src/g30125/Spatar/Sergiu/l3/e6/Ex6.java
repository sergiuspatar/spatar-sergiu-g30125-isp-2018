package g30125.Spatar.Sergiu.l3.e6;
public class Ex6 {
	private int x, y; 
	public Ex6() 
	{
		x=y=0;
	}
	public Ex6(int x,int y)	
	{
		this.x=x;
		this.y=y;
	}
	void setX(int k){ 	
    		this.x=x;
    }
	void setY(int z){
		this.y=y;	
	}
    int getx(){
    	return x;
    }
    int gety(){
    	return y;	
    }
     void setXY(int x,int y){
    	this.x=x;
    	this.y=y;
    }
    public String toString()
	{
		return "("+x+", "+y+")";
	}
    public double distance(Ex6 a)
	{
		return Math.sqrt((x-a.getx())*(x-a.getx())+(y-a.gety())*(y-a.gety()));	
	}
}