/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l8.e4;

import java.io.Serializable;

public class Car implements Serializable{
	
	private String model;
	private double price;
	public Car(String model,double price) {
		this.model=model;
		this.price=price;
	}
	public String getModel() {
		return model;
	}
	public double getPrice() {
		return price;
	}
	@Override
	public String toString() {
		return "Model=" + getModel() + ", pret=" + getPrice() + "]";
	}
	
}
