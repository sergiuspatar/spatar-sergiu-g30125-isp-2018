/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l5.e1;
public class Circle extends Shape {
protected double radius;
public Circle()
{
	radius=5;
}
public Circle(double radius)
{
	radius=2.0;
}
public Circle(double radius,String color,boolean filled)
{
	radius=4;
	color="circ";
	filled=true;
}
public double getRadius() {
	return radius;
}
public void setRadius(double radius) {
	this.radius = radius;
}
@Override
public String toString() {
	return "Circle [radius=" + radius + "]";
}
public double getArea()
{
	return 3.14*radius*radius;
}
public double getPerimeter()
{
	return 2*3.14*radius;
}

}
