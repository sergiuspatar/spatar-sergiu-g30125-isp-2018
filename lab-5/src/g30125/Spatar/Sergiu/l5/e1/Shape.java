/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l5.e1;

abstract  class Shape {
protected String color;
protected boolean filled;
public Shape ()
{
	color="red";
	filled=true;
}
public Shape (String color,boolean filled)
{
	color="green";
	filled=false;
}
public String getColor() {
	return color;
}
public void setColor(String color) {
	this.color = color;
}
public boolean isFilled() {
	return filled;
}
public void setFilled(boolean filled) {
	this.filled = filled;
}
abstract double getArea();
abstract double getPerimeter();
@Override
public String toString() {
	return "Shape [color=" + color + ", filled=" + filled + "]";
}

}

