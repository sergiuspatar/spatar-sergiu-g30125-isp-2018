/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g30125.Spatar.Sergiu.l5.e2;
public class ProxyImage implements Image {
	 private RealImage realImage;
	 private RotatedImage rotatedimage ;
	   private String fileName;
	 
	   public ProxyImage(String fileName){
	   
	   		if(fileName=="Real")
	   		{
	   		 realImage = new RealImage(fileName);
	   		 realImage.display();
	   		}
	   		else
	   		{
	   			if(fileName=="Rotated")
	   			{
	   			 rotatedimage = new RotatedImage(fileName);
		   		 rotatedimage.display();
	   			}
	   		}
	   }
		     
		   
	   @Override
	   public void display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      realImage.display();
	   }
}
